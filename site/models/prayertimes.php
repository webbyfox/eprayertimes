<?php
/**
 * @package		KA Prayer Times
 * @author		Rizwan Mansuri http://www.webbyfox.co.uk
 * @copyright 	Copyright (C) 2015 Rizwan Mansuri - http://www.webbyfox.co.uk
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.modellist');

class eprayertimesModelPrayertimes extends JModelList {
	public function __construct($config = array()) {
		parent::__construct($config);
	}

	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication();
		$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'));
		$this->setState('list.limit', $limit);

		$limitstart = JFactory::getApplication()->input->getInt('limitstart', 0);
		$this->setState('list.start', $limitstart);

		$menuParams = $app->getParams();
		$mid = $this->getUserStateFromRequest($this->context . '.filter.mid', 'filter_mid', '', 'string');
		if (!$mid) {
			$mid = $menuParams->get('mosque_id');
		}

		$this->setState('filter.mid', $mid);
		$month = $this->getUserStateFromRequest($this->context . '.filter.month', 'filter_month', '', 'string');
		if (!$month) {
			$month = date("mY");
		}

		$this->setState('filter.month', $month);
		$params = JComponentHelper::getParams('com_eprayertimes');
		$this->setState('params', $params);

		parent::populateState($ordering, $direction);
	}

	protected function getListQuery() {
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select($this->getState('list.select', 'a.*'));
		$query->from('`#__eprayertimes_prayertimes` AS a');

		$published = $this->getState('filter.state');
		if (is_numeric($published)) {
			$query->where('a.state = ' . (int) $published);
		} else if ($published === '') {
			$query->where('(a.state IN (0, 1))');
		}

		$mid = $this->getState('filter.mid', null, 'int');
		if (!empty($mid)) {
			$query->where('(a.mid=' . $db->Quote($mid) . ')');
		}
		$month = $this->getState('filter.month', null, 'int');
		if (!empty($month)) {
			$query->where('(date_format(a.date, \'%m%Y\')=' . $db->Quote($month) . ')');
		}

		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('a.id = ' . (int) substr($search, 3));
			} else {
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.name LIKE ' . $search . ' )');
			}
		}

		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
		//if ($orderCol && $orderDirn) {
		//    $query->order($db->escape($orderCol . ' ' . $orderDirn));
		//} else {
		$query->order($db->escape('a.date asc'));
		//}
		return $query;
	}
	function getRamadhanData() {
		if (empty($this->_ramadhan_data)) {
			$query = $this->_buildRamadhanQuery();
			$this->_ramadhan_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}
		return $this->_ramadhan_data;
	}
	function _buildRamadhanQuery() {
		$mosque = $this->_getMosque();
		$query = 'SELECT * FROM #__eprayertimes_' . $mosque . ''
		. ' WHERE date BETWEEN ' . $this->_db->Quote($this->_ramadhan_sdate) . ' AND ' . $this->_db->Quote($this->_ramadhan_fdate)
		. ' ORDER BY date';
		return $query;
	}
	function _getMosque() {
		$app = JFactory::getApplication();
		$option = JRequest::getCmd('option');
		//$filter_mosque	= $app->getUserStateFromRequest( $option.'filter_mosque', 'filter_mosque', 0, 'int' );

		$mosque = JRequest::getInt('mosque', 0);
		if (!$mosque) {
			$mosque = $this->getState('filter_mosque', '0');
		}
		if ($mosque) {
			return $mosque;
		} else {
			$query = "SELECT m.id AS value, m.name AS text"
			. "\n FROM #__eprayertimes_mosques as m"
			. "\n WHERE m.published = 1"
			. "\n GROUP BY m.id"
			. "\n ORDER BY m.name";
			$this->_db->setQuery($query);
			$list = $this->_db->loadObjectlist();

			If (!$list) {
				$app = JFactory::getApplication();
				$app->redirect('index.php?option=com_eprayertimes&view=prayertimes', JText::_('CREATE_A_MOSQUE_FIRST'));
			}

			return $list[0]->value;
		}
	}
	function getMosqueData() {
		$app = JFactory::getApplication();
		$option = JRequest::getCmd('option');
		//$filter_mosque	= $app->getUserStateFromRequest( $option.'filter_mosque', 'filter_mosque', 0, 'int' );
		$mosque = JRequest::getInt('mosque', 0);
		if (!$mosque) {
			$mosque = $this->getState('filter_mosque', '0');
		}

		$query = "SELECT m.* "
		. "\n FROM #__eprayertimes_mosques m"
		. "\n WHERE m.published = 1"
		. "\n AND m.id = " . $mosque;
		$this->_db->setQuery($query);
		$mosqueData = $this->_db->loadObject();

		If (!$mosqueData) {
			$app = JFactory::getApplication();
			$app->redirect('index.php', JText::_('CREATE_A_MOSQUE_FIRST'));
		}

		$this->_ramadhan_status = $mosqueData->ramadhan_status;
		$this->_ramadhan_sdate = $mosqueData->ramadhan_sdate;
		$this->_ramadhan_fdate = $mosqueData->ramadhan_fdate;

		return $mosqueData;
	}
	function getMosques() {
		$query = "SELECT m.id AS value, m.name AS text"
		. "\n FROM #__eprayertimes_mosques as m"
		. "\n WHERE m.published = 1"
		. "\n GROUP BY m.id"
		. "\n ORDER BY m.name";
		$this->_db->setQuery($query);
		$list = $this->_db->loadObjectlist();

		If (!$list) {
			$app = JFactory::getApplication();
			$app->redirect('index.php', JText::_('CREATE_A_MOSQUE_FIRST'));
		}
		return $list;
	}
	function getMonths() {
		$option = JRequest::getCmd('option');
		$app = JFactory::getApplication();
		$db = &JFactory::getDBO();

		$mosque = $this->_getMosque();

		$db = &JFactory::getDBO();
		$query = 'SELECT date_format(date, \'%m%Y\') as value, date_format(date, \'%M %Y\') as text '
		. ' FROM #__eprayertimes_' . $mosque
		. ' GROUP BY value'
		. ' ORDER BY date';
		$db->setQuery($query);
		$monthsResult = $db->loadObjectList();
//        If (!$monthsResult) {
		//            $app = JFactory::getApplication();
		//            $app->redirect('index.php?option=com_eprayertimes&view=uploaddata', JText::_('UPLOAD_DATA_FIRST'));
		//        }
		return $monthsResult;
	}
}
