<?php
/**
 * @package		KA Prayer Times
 * @author		Rizwan Mansuri http://www.webbyfox.co.uk
 * @copyright 	Copyright (C) 2015 Rizwan Mansuri - http://www.webbyfox.co.uk
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
defined('_JEXEC') or die;
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$mosques = EprayertimesHelper::getMosquesListFromDb();
$months = EprayertimesHelper::getMonths();

if ($this->params->get('show_page_heading')):
	echo '<h1>' . $this->escape($this->params->get('page_heading')) . '</h1>';
else:
	$mosque = EprayertimesHelper::getMosquesListFromDb($this->state->get('filter.mid'));
	echo '<h1>' . $this->escape($mosque[0]->text) . '</h1>';
endif;
?>
<style>
#eprayertimes tr.today {
    background-color: <?php echo $this->params->get('today_row_bg', 'lightgreen');?>;
    color: <?php echo $this->params->get('today_row_fg', '');?>;
}
</style>
<form action="<?php echo JRoute::_('index.php?option=com_eprayertimes&view=prayertimes');?>" method="post" name="adminForm" id="adminForm">
    <div id="filter-bar" class="btn-toolbar">
        <div class="btn-group pull-left">
            <select name="filter_mid" class="inputbox" onchange="this.form.submit()">
                <?php echo JHtml::_('select.options', $mosques, 'value', 'text', $this->state->get('filter.mid'));?>
            </select>
        </div>
        <div class="btn-group pull-left">
            <select name="filter_month" class="inputbox" onchange="this.form.submit()">
                <option value="<?php echo date("mY");?>"><?php echo JText::_('COM_EPRAYERTIMES_SELECT_CURRENT_MONTH');?></option>
                <?php echo JHtml::_('select.options', $months, 'value', 'text', $this->state->get('filter.month'));?>
            </select>
        </div>
    </div>
    <input type="hidden" name="return" value="<?php //echo $this->return_page;?>" />
    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token');?>
</form>
<div id="eprayertimes">
<?php
if ($this->items): ?>
        <table class="table eprayertimes-table table-first-column-check">
            <thead>
                <tr>
                    <?php if ($this->params->get('show_date')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_DATE') . '</th>';
}
?>
                    <?php if ($this->params->get('show_day')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_DAY') . '</th>';
}
?>
                    <?php if ($this->params->get('show_fajr_begins')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_FAJAR_BEGINS') . '</th>';
}
?>
                    <?php if ($this->params->get('show_fajr_iqama')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_FAJAR_JAMAAT') . '</th>';
}
?>
                    <?php if ($this->params->get('show_sunrise')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_SUNRISE') . '</th>';
}
?>
                    <?php if ($this->params->get('show_dhuhr_begins')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_DHUHR_BEGINS') . '</th>';
}
?>
                    <?php if ($this->params->get('show_dhuhr_iqama')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_DHUHR_JAMAAT') . '</th>';
}
?>
                    <?php if ($this->params->get('show_asr_begins')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_ASR_BEGINS') . '</th>';
}
?>
                    <?php if ($this->params->get('show_asr_iqama')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_ASR_JAMAAT') . '</th>';
}
?>
                    <?php if ($this->params->get('show_maghrib_begins')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_MAGHRIB_BEGINS') . '</th>';
}
?>
                    <?php if ($this->params->get('show_maghrib_iqama')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_MAGHRIB_JAMAAT') . '</th>';
}
?>
                    <?php if ($this->params->get('show_isha_begins')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_ISHA_BEGINS') . '</th>';
}
?>
                    <?php if ($this->params->get('show_isha_iqama')) {
	echo '<th>' . JText::_('COM_EPRAYERTIMES_ISHA_JAMAAT') . '</th>';
}
?>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($this->items as $item):
	if ($item->date == date("Y-m-d")) {
		echo "<tr class='today'>";
	} else {
		echo '<tr>';
	}

	?>
		                    <?php if ($this->params->get('show_date')) {
		echo '<td>' . date($this->params->get('date_format'), strtotime($item->date)) . '</td>';
	}
	?>
		                    <?php if ($this->params->get('show_day')) {
		echo '<td>' . $weekday = date($this->params->get('day_format'), strtotime($item->date)) . '</td>';
	}
	?>
		                    <?php if ($this->params->get('show_fajr_begins')) {
		echo '<td>' . $item->fajr_begins . '</td>';
	}
	?>
		                    <?php if ($this->params->get('show_fajr_iqama')) {
		echo '<td>' . $item->fajr_iqama . '</td>';
	}
	?>
		                    <?php if ($this->params->get('show_sunrise')) {
		echo '<td>' . $item->sunrise . '</td>';
	}
	?>
		                    <?php if ($this->params->get('show_dhuhr_begins')) {
		echo '<td>' . $item->dhuhr_begins . '</td>';
	}
	?>
		                    <?php if ($this->params->get('show_dhuhr_iqama')) {
		echo '<td>' . $item->dhuhr_iqama . '</td>';
	}
	?>
		                    <?php if ($this->params->get('show_asr_begins')) {
		echo '<td>' . $item->asr_begins . '</td>';
	}
	?>
		                    <?php if ($this->params->get('show_asr_iqama')) {
		echo '<td>' . $item->asr_iqama . '</td>';
	}
	?>
		                    <?php if ($this->params->get('show_maghrib_begins')) {
		echo '<td>' . $item->maghrib_begins . '</td>';
	}
	?>
		                    <?php if ($this->params->get('show_maghrib_iqama')) {
		echo '<td>' . $item->maghrib_iqama . '</td>';
	}
	?>
		                    <?php if ($this->params->get('show_isha_begins')) {
		echo '<td>' . $item->isha_begins . '</td>';
	}
	?>
		                    <?php if ($this->params->get('show_isha_iqama')) {
		echo '<td>' . $item->isha_iqama . '</td>';
	}
	?>
		                </tr>
		            <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="20"><?php echo COM_EPRAYERTIMES_VERSION_LINK;?></td>
                </tr>
            </tfoot>
        </table>

<?php else: ?>
    <p>There are no items in the list.</p>
<?php endif;?>
</div>