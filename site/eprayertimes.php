<?php
/**
 * @version     1.0.0
 * @package     com_eprayertimes
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Rizwan <support@webbyfox.co.uk> - http://www.webbyfox.co.uk
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

require_once JPATH_ADMINISTRATOR . '/components/com_eprayertimes/helpers/eprayertimes.php';

$extInfo = EprayertimesHelper::getExtensionInfo('eprayertimes');

define('COM_EPRAYERTIMES_VERSION', $extInfo['version']);
define('COM_HOLYQURAN_CREATIONDATE', isset($extInfo['creationDate']) ? $extInfo['creationDate'] : $extInfo['creationdate']);
define('COM_EPRAYERTIMES_VERSION_LINK', '<a href="http://www.webbyfox.co.uk/joomla/downloads/item/14-ka-prayer-times.html" target="_blank">e Prayer Times ' . $extInfo['version'] . '</a>');

$lang = JFactory::getLanguage();
$lang->load('com_eprayertimes', JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_eprayertimes/assets/css/eprayertimes.css');

// Execute the task.
$controller = JControllerLegacy::getInstance('Eprayertimes');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
