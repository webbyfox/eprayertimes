<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');
class JFormFieldCountries extends JFormField {
	protected $type = 'Countries';

	public function getInput() {
		$options = '<option value="">' . JText::_('COM_EPRAYERTIMES_SELECT_COUNTRY') . '</option>';
		$group = array();
		$countries = EprayertimesHelper::getCountries();
		foreach ($countries as $key => $value) {
			($key == $this->value) ? $selected = ' selected="selected"' : $selected = '';
			$options .= '<option value="' . $key . '"' . $selected . '>' . $value . '</option>';
		}
		return '<select id="' . $this->id . '" name="' . $this->name . '">' . $options . '</select>';
	}
}