<?php
/**
 * @package		KA Prayer Times
 * @author		Rizwan Mansuri http://www.webbyfox.co.uk
 * @copyright 	Copyright (C) 2015 Rizwan Mansuri - http://www.webbyfox.co.uk
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.modeladmin');

class EprayertimesModelmosque extends JModelAdmin {
	protected $text_prefix = 'COM_EPRAYERTIMES';

	public function getTable($type = 'Mosque', $prefix = 'EprayertimesTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}
	public function getForm($data = array(), $loadData = true) {
		$app = JFactory::getApplication();
		$form = $this->loadForm('com_eprayertimes.mosque', 'mosque', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}
	protected function loadFormData() {
		$data = JFactory::getApplication()->getUserState('com_eprayertimes.edit.mosque.data', array());
		if (empty($data)) {
			$data = $this->getItem();
		}
		return $data;
	}
	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {
		}
		return $item;
	}
	protected function prepareTable($table) {
		jimport('joomla.filter.output');
		if (empty($table->id)) {
			if (@$table->ordering === '') {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__eprayertimes_mosques');
				$max = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}
}