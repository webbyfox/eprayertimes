<?php
/**
 * @package		KA Prayer Times
 * @author		Rizwan Mansuri http://www.webbyfox.co.uk
 * @copyright 	Copyright (C) 2015 Rizwan Mansuri - http://www.webbyfox.co.uk
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.modeladmin');
jimport('joomla.application.component.modelform');
jimport('joomla.filesystem.folder');

class EprayertimesModelImport extends JModelAdmin {
	protected $text_prefix = 'COM_EPRAYERTIMES';
	var $params = null;
	var $upload_dir = null;

	public function __construct($config = array()) {
		parent::__construct($config);
		$this->params = JComponentHelper::getParams('com_eprayertimes');
		$this->upload_dir = JPATH_SITE . '/administrator/components/com_eprayertimes/uploads/';
	}
	public function getTable($type = 'Import', $prefix = 'EprayertimesTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}
	public function getForm($data = array(), $loadData = true) {
		$app = JFactory::getApplication();
		$form = $this->loadForm('com_eprayertimes.import', 'import', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}
	protected function loadFormData() {
		$data = JFactory::getApplication()->getUserState('com_eprayertimes.edit.import.data', array());
		if (empty($data)) {
			$data = $this->getItem();
		}
		return $data;
	}
//	public function getItem($pk = null) {
	//		if ($item = parent::getItem($pk)) {
	//		}
	//		return $item;
	//	}
	protected function prepareTable($table) {
		jimport('joomla.filter.output');
		if (empty($table->id)) {
			if (@$table->ordering === '') {
				$this->_db->setQuery('SELECT MAX(ordering) FROM #__eprayertimes_import');
				$max = $this->_db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}
	function validatefile($file) {
		$app = JFactory::getApplication();

		$allowed_mime = explode(',', $this->params->get('allowedmime'));
		$illegal_mime = explode(',', $this->params->get('illegalmime'));
		if (function_exists('finfo_open') && $this->params->get('checkmime', 1)) {
			$finfo = finfo_open(FILEINFO_MIME);
			$tmp_name = $file['tmp_name'];

			$type = finfo_file($finfo, $tmp_name);
			if (strlen($type) && !in_array($type, $allowed_mime) && in_array($type, $illegal_mime)) {
				$app->enqueueMessage(JText::_('COM_EPRAYERTIMES_INVALID_MIME' . ': ' . $mimetype), 'error');
				return false;
			}
			finfo_close($finfo);
		} elseif (function_exists('mime_content_type')) {
			// we have mime magic
			$type = mime_content_type($file['tmp_name']);
			if (strlen($type) && !in_array($type, $allowed_mime) && in_array($type, $illegal_mime)) {
				$app->enqueueMessage(JText::_('COM_EPRAYERTIMES_INVALID_MIME' . ': ' . $mimetype), 'error');
				return false;
			}
		}
		if ($this->params->get('checkextns')) {
			//$allowedExtns =  array('gif', 'png' ,'jpg', 'txt');
			$allowedExtns = explode(',', 'csv');
			$ext = strtolower(pathinfo($file['newname'], PATHINFO_EXTENSION));
			if (!in_array($ext, $allowedExtns)) {
				$app->enqueueMessage(JText::_('COM_EPRAYERTIMES_FILE_EXTENSION_TYPE_NOT ALLWED' . ': ' . $ext), 'error');
				return false;
			}
		}
		if ($file['newname'] != '') {
			$src = $file['tmp_name']['file'];
			$dest = $this->upload_dir . $file['newname'];
			$upload = JFile::upload($src, $dest, false);
			if ($upload) {
				return true;
			} else {
				$app->enqueueMessage(JText::_('COM_EPRAYERTIMES_ERROR_IN_FILE_UPLOAD'), 'error');
				return false;
			}
		}
		return false;
	}
	function parseCSVFile($data) {
		$app = JFactory::getApplication();
		$option = JRequest::getVar('option');
		$delimiter = ',';

		if (eregi("(\.(csv))$", $data['file'])) {
			$queries = array();
			$dbDriver = strtolower($this->_db->name);
			if ($dbDriver == 'mysqli') {
				$dbDriver = 'mysql';
			}
			$dbCharset = ($this->_db->hasUTFSupport()) ? 'utf8' : '';
			$fCharset = 'utf8';
			$fDriver = 'mysql';

			if ($fCharset == $dbCharset && $fDriver == $dbDriver) {
				// Get the name of the sql file to process
				$csvfile = $this->upload_dir . $data['file'];

				// Check that sql files exists before reading. Otherwise raise error for rollback
				if (!file_exists($csvfile)) {
					$app->enqueueMessage(JText::_('COM_EPRAYERTIMES_FILE_DOES_NOT_EXIST') . $csvfile, 'error');
					return false;
				}
				$buffer = file_get_contents($csvfile);
				// Graceful exit and rollback if read not successful
				if ($buffer === false) {
					$app->enqueueMessage(JText::_('COM_EPRAYERTIMES_COULD_NOT_OPEN_FILE') . $csvfile, 'error');
					return false;
				}

				$arrData = array();
				$resFileHandler = fopen($csvfile, 'r');

				if ($csvFormat == 'masjid_albirr_format') {
					$countQueries = $this->masjidAlbirrImport($resFileHandler, $data);
				} else {
					$countQueries = 0;
					$arrData = array();
					while ($arrData = fgetcsv($resFileHandler, 1000, ",", "\"")) {
						if (preg_match('`^\d{1,2}/\d{1,2}/\d{4}$`', $arrData[1])) {
							$date = $arrData[1];

							$dateArray = explode('/', $date);
							$fDate = $dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0];
							$fDate = date("Y-m-d", strtotime($fDate));

							$table = '#__eprayertimes_prayertimes';
							$query = 'SELECT date FROM ' . $table . ' WHERE date = ' . $this->_db->Quote($fDate) . ' AND mid = ' . $this->_db->Quote($data['id']);
							$this->_db->setQuery($query);
							$dateExists = $this->_db->loadResult();

							if ($dateExists && $data['skipdata'] == 'skip') {
								$app->enqueueMessage(JText::_('PRAYER_TIMES_FOR_THIS_DATE_SKIPPED') . ' ' . $date, 'Warning');
							} else if ($dateExists && $data['skipdata'] = 'update') {
								$query = 'UPDATE ' . $table
								. ' SET '
								. 'mid = ' . $this->_db->Quote($data['id']) . ', '
								. 'date = ' . $this->_db->Quote($fDate) . ', '
								. 'fajr_begins = ' . $this->_db->Quote($arrData[2]) . ', '
								. 'fajr_iqama = ' . $this->_db->Quote($arrData[3]) . ', '
								. 'sunrise = ' . $this->_db->Quote($arrData[4]) . ', '
								. 'dhuhr_begins = ' . $this->_db->Quote($arrData[5]) . ', '
								. 'dhuhr_iqama = ' . $this->_db->Quote($arrData[6]) . ', '
								. 'asr_begins = ' . $this->_db->Quote($arrData[7]) . ', '
								. 'asr_iqama = ' . $this->_db->Quote($arrData[8]) . ', '
								. 'maghrib_begins = ' . $this->_db->Quote($arrData[9]) . ', '
								. 'maghrib_iqama = ' . $this->_db->Quote($arrData[10]) . ', '
								. 'isha_begins = ' . $this->_db->Quote($arrData[11]) . ', '
								. 'isha_iqama = ' . $this->_db->Quote($arrData[12])
								. ' WHERE ' . 'date = ' . $this->_db->Quote($fDate)
								. ' AND mid = ' . $this->_db->Quote($data['id'])
								;
								$query = trim($query);
								$this->_db->setQuery($query);
								if (!$this->_db->query()) {
									//$app->redirect("index.php?option=$option&view=uploaddata", JText::_('SQL Error')." ".$this->_db->stderr(true));
									$app->enqueueMessage(JText::_('Error while uploading times for date.') . ' ' . $date . ' ' . JText::_('SQL Error') . ' ' . $this->_db->stderr(true), 'error');
									//return false;
								}
								$app->enqueueMessage(JText::_('Prayer times updated for date: ') . ' ' . $date, 'success');
								$countQueries++;
							} else {
								$query = 'INSERT INTO ' . $table
								. ' ('
								. 'mid, '
								. 'date, '
								. 'fajr_begins, '
								. 'fajr_iqama, '
								. 'sunrise, '
								. 'dhuhr_begins, '
								. 'dhuhr_iqama, '
								. 'asr_begins, '
								. 'asr_iqama, '
								. 'maghrib_begins, '
								. 'maghrib_iqama, '
								. 'isha_begins, '
								. 'isha_iqama'
								. ') VALUES ('
								. $this->_db->Quote($data['id']) . ', '
								. $this->_db->Quote($fDate) . ', '
								. $this->_db->Quote($arrData[2]) . ', '
								. $this->_db->Quote($arrData[3]) . ', '
								. $this->_db->Quote($arrData[4]) . ', '
								. $this->_db->Quote($arrData[5]) . ', '
								. $this->_db->Quote($arrData[6]) . ', '
								. $this->_db->Quote($arrData[7]) . ', '
								. $this->_db->Quote($arrData[8]) . ', '
								. $this->_db->Quote($arrData[9]) . ', '
								. $this->_db->Quote($arrData[10]) . ', '
								. $this->_db->Quote($arrData[11]) . ', '
								. $this->_db->Quote($arrData[12])
								. ')'
								;
								$query = trim($query);
								$this->_db->setQuery($query);
								if (!$this->_db->query()) {
									$app->enqueueMessage(JText::_('Error while inserting times for this date.') . ' ' . $date . ' ' . JText::_('SQL Error') . ' ' . $this->_db->stderr(true), 'error');
									//return false;
								}
								$countQueries++;
							}
						}
					}
				}
			}
			$app->enqueueMessage(JText::_('Successfull queries: ') . $countQueries, '');
			return true;
		}
	}
	function getSampleFiles() {
		$folderPath = JPATH_COMPONENT . '/samples';
		$folderExists = JFolder::exists($folderPath);
		$samples = array();
		if ($folderExists) {
			$fileCount = 0;
			$dir = opendir($folderPath); # Open the path
			while ($file = readdir($dir)) {
				if (preg_match('/.csv/i', $file)) {
					$samples[] = $file;
					$fileCount++;
				}
			}
		}
		return $samples;
	}
}