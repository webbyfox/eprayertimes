CREATE TABLE IF NOT EXISTS `#__eprayertimes_mosques` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`type`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`street`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`county`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`city`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`country`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`postcode`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`age`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`wudhu_space`  int(11) NOT NULL ,
`toilets`  int(11) NOT NULL ,
`parking_space`  int(11) NOT NULL ,
`staff_num`  int(11) NOT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`lastsync`  datetime NOT NULL ,
`approved`  tinyint(4) NOT NULL ,
`featured`  tinyint(4) NOT NULL ,
`default`  tinyint(4) NOT NULL ,
`ramadhan_status`  tinyint(4) NOT NULL ,
`ramadhan_sdate`  date NOT NULL ,
`ramadhan_fdate`  date NOT NULL ,
`decimal_latitude`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`decimal_longitude`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`ordering`  int(11) NOT NULL ,
`state`  tinyint(1) NOT NULL DEFAULT 1 ,
`checked_out`  int(11) NOT NULL ,
`checked_out_time`  datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ,
`created_by`  int(11) NOT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=4
CHECKSUM=0
ROW_FORMAT=DYNAMIC
DELAY_KEY_WRITE=0
;
CREATE TABLE IF NOT EXISTS `#__eprayertimes_prayertimes` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`mid`  int(10) NOT NULL ,
`date`  date NOT NULL DEFAULT '0000-00-00' ,
`fajr_begins`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`fajr_iqama`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`sunrise`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`dhuhr_begins`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`dhuhr_iqama`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`asr_begins`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`asr_iqama`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`maghrib_begins`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`maghrib_iqama`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`isha_begins`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`isha_iqama`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`state`  tinyint(1) NOT NULL DEFAULT 1 ,
`checked_out`  int(11) NOT NULL ,
`checked_out_time`  datetime NOT NULL ,
`created_by`  int(11) NOT NULL ,
PRIMARY KEY (`id`),
INDEX `Date` USING BTREE (`date`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=793
CHECKSUM=0
ROW_FORMAT=DYNAMIC
DELAY_KEY_WRITE=0
;
CREATE TABLE IF NOT EXISTS `#__eprayertimes_import` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`ordering`  int(11) NOT NULL ,
`state`  tinyint(1) NOT NULL DEFAULT 1 ,
`checked_out`  int(11) NOT NULL ,
`checked_out_time`  datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ,
`created_by`  int(11) NOT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1
CHECKSUM=0
ROW_FORMAT=FIXED
DELAY_KEY_WRITE=0
;
