<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.view');

class EprayertimesViewDashboard extends JViewLegacy {
	protected $item;
	protected $items;
	protected $pagination;
	protected $state;

	public function display($tpl = null) {
		$layout = $this->getLayout();

		$this->params = JComponentHelper::getParams('com_eprayertimes');

		$model = &$this->getModel();

		if (count($errors = $this->get('Errors'))) {
			throw new Exception(implode("\n", $errors));
		}
		$layout = $this->getLayout();
		if ($layout == 'default') {
			$this->addToolbar();
		}
		EprayertimesHelper::addSubmenu('dashboard');
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}
	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/eprayertimes.php';
		$canDo = EprayertimesHelper::getActions();
		JToolBarHelper::title(JText::_('COM_EPRAYERTIMES_TITLE_DASHBOARD'), 'dashboard.png');

		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_eprayertimes');
		}
	}

}
