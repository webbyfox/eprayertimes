<?php
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$document = JFactory::getDocument();
$document->addStyleSheet('components/com_eprayertimes/assets/css/eprayertimes.css');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$canOrder = $user->authorise('core.edit.state', 'com_eprayertimes');
$saveOrder = $listOrder == 'a.ordering';
if ($saveOrder) {
	$saveOrderingUrl = 'index.php?option=com_eprayertimes&task=prayertimes.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'prayertimeList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();

$mosques = EprayertimesHelper::getMosquesListFromDb();
$months = EprayertimesHelper::getMonths();
?>
<script type="text/javascript">
	Joomla.orderTable = function() {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder;?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>
<?php
//Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar)) {
	$this->sidebar .= $this->extra_sidebar;
}
?>
<form action="<?php echo JRoute::_('index.php?option=com_eprayertimes&view=prayertimes');?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar;?>
	</div>
	<div id="j-main-container" class="span10">
<?php else: ?>
	<div id="j-main-container">
<?php endif;?>
		<div id="filter-bar" class="btn-toolbar">
            <div class="btn-group pull-left">
                <select name="filter_mid" class="inputbox" onchange="this.form.submit()">
                    <?php echo JHtml::_('select.options', $mosques, 'value', 'text', $this->state->get('filter.mid'));?>
                </select>
            </div>
            <div class="btn-group pull-left">
                <select name="filter_month" class="inputbox" onchange="this.form.submit()">
                    <?php echo JHtml::_('select.options', $months, 'value', 'text', $this->state->get('filter.month'));?>
                </select>
            </div>
		</div>
		<div class="clearfix"> </div>
		<table class="table table-striped" id="prayertimeList">
			<thead>
				<tr>
                <?php if (isset($this->items[0]->ordering)): ?>
					<th width="1%" class="nowrap center hidden-phone">
						<?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'a.ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING');?>
					</th>
                <?php endif;?>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL');?>" onclick="Joomla.checkAll(this)" />
					</th>
                <?php if (isset($this->items[0]->state)): ?>
					<th width="1%" class="nowrap center">
						<?php echo JHtml::_('grid.sort', 'JSTATUS', 'a.state', $listDirn, $listOrder);?>
					</th>
                <?php endif;?>

				<th class='left'><?php echo JHtml::_('grid.sort', 'COM_EPRAYERTIMES_PRAYERTIMES_DATE', 'a.date', $listDirn, $listOrder);?></th>
				<th class='left'><?php echo JHtml::_('grid.sort', 'COM_EPRAYERTIMES_PRAYERTIMES_FAJR_BEGINS', 'a.fajr_begins', $listDirn, $listOrder);?></th>
                <th class='left'><?php echo JHtml::_('grid.sort', 'COM_EPRAYERTIMES_PRAYERTIMES_FAJR_IQAMA', 'a.fajr_iqama', $listDirn, $listOrder);?></th>
                <th class='left'><?php echo JHtml::_('grid.sort', 'COM_EPRAYERTIMES_PRAYERTIMES_SUNRISE', 'a.sunrise', $listDirn, $listOrder);?></th>
                <th class='left'><?php echo JHtml::_('grid.sort', 'COM_EPRAYERTIMES_PRAYERTIMES_DHUHR_BEGINS', 'a.dhuhr_begins', $listDirn, $listOrder);?></th>
                <th class='left'><?php echo JHtml::_('grid.sort', 'COM_EPRAYERTIMES_PRAYERTIMES_DHUHR_IQAMA', 'a.dhuhr_iqama', $listDirn, $listOrder);?></th>
                <th class='left'><?php echo JHtml::_('grid.sort', 'COM_EPRAYERTIMES_PRAYERTIMES_ASR_BEGINS', 'a.asr_begins', $listDirn, $listOrder);?></th>
                <th class='left'><?php echo JHtml::_('grid.sort', 'COM_EPRAYERTIMES_PRAYERTIMES_ASR_IQAMA', 'a.asr_iqama', $listDirn, $listOrder);?></th>
                <th class='left'><?php echo JHtml::_('grid.sort', 'COM_EPRAYERTIMES_PRAYERTIMES_MAGHRIB_BEGINS', 'a.maghrib_begins', $listDirn, $listOrder);?></th>
                <th class='left'><?php echo JHtml::_('grid.sort', 'COM_EPRAYERTIMES_PRAYERTIMES_MAGHRIB_IQAMA', 'a.maghrib_iqama', $listDirn, $listOrder);?></th>
                <th class='left'><?php echo JHtml::_('grid.sort', 'COM_EPRAYERTIMES_PRAYERTIMES_ISHA_BEGINS', 'a.isha_begins', $listDirn, $listOrder);?></th>
                <th class='left'><?php echo JHtml::_('grid.sort', 'COM_EPRAYERTIMES_PRAYERTIMES_ISHA_IQAMA', 'a.isha_iqama', $listDirn, $listOrder);?></th>
                <!--<th class='left'><?php //echo JText::_('COM_EPRAYERTIMES_PRAYERTIMES_SAVE'); ?></th>-->

                <?php if (isset($this->items[0]->id)): ?>
					<th width="1%" class="nowrap center hidden-phone">
						<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder);?>
					</th>
                <?php endif;?>
				</tr>
			</thead>
			<tfoot>
                <?php
if (isset($this->items[0])) {
	$colspan = count(get_object_vars($this->items[0]));
} else {
	$colspan = 10;
}
?>
			<tr>
				<td colspan="<?php echo $colspan?>">
					<?php echo $this->pagination->getListFooter();?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php foreach ($this->items as $i => $item):
	$ordering = ($listOrder == 'a.ordering');
	$canCreate = $user->authorise('core.create', 'com_eprayertimes');
	$canEdit = $user->authorise('core.edit', 'com_eprayertimes');
	$canCheckin = $user->authorise('core.manage', 'com_eprayertimes');
	$canChange = $user->authorise('core.edit.state', 'com_eprayertimes');
	?>
					<tr class="row<?php echo $i % 2;?>">

	                <?php if (isset($this->items[0]->ordering)): ?>
						<td class="order nowrap center hidden-phone">
						<?php if ($canChange):
		$disableClassName = '';
		$disabledLabel = '';
		if (!$saveOrder):
			$disabledLabel = JText::_('JORDERINGDISABLED');
			$disableClassName = 'inactive tip-top';
		endif;?>
								<span class="sortable-handler hasTooltip <?php echo $disableClassName?>" title="<?php echo $disabledLabel?>">
									<i class="icon-menu"></i>
								</span>
								<input type="text" style="display:none" name="order[]" size="5" value="<?php echo $item->ordering;?>" class="width-20 text-area-order " />
							<?php else: ?>
							<span class="sortable-handler inactive" >
								<i class="icon-menu"></i>
							</span>
						<?php endif;?>
						</td>
	                <?php endif;?>
						<td class="center hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $item->id);?>
						</td>
	                <?php if (isset($this->items[0]->state)): ?>
						<td class="center">
							<?php echo JHtml::_('jgrid.published', $item->state, $i, 'prayertimes.', $canChange, 'cb');?>
						</td>
	                <?php endif;?>
					<td>
					<?php if (isset($item->checked_out) && $item->checked_out): ?>
						<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'prayertimes.', $canCheckin);?>
					<?php endif;?>
					<?php if ($canEdit): ?>
						<a href="<?php echo JRoute::_('index.php?option=com_eprayertimes&task=prayertime.edit&id=' . (int) $item->id);?>">
						<?php echo $this->escape($item->date);?></a>
					<?php else: ?>
						<?php echo $this->escape($item->date);?>
					<?php endif;?>
					</td>
					<td><?php echo $item->fajr_begins;?></td>
	                <td><?php echo $item->fajr_iqama;?></td>
	                <td><?php echo $item->sunrise;?></td>
	                <td><?php echo $item->dhuhr_begins;?></td>
	                <td><?php echo $item->dhuhr_iqama;?></td>
	                <td><?php echo $item->asr_begins;?></td>
	                <td><?php echo $item->asr_iqama;?></td>
	                <td><?php echo $item->maghrib_begins;?></td>
	                <td><?php echo $item->maghrib_iqama;?></td>
	                <td><?php echo $item->isha_begins;?></td>
	                <td><?php echo $item->isha_iqama;?></td>
	                <!--<td><?php //echo $item->save; ?></td>-->
	                <?php if (isset($this->items[0]->id)): ?>
	                <td class="center hidden-phone"><?php echo (int) $item->id;?></td>
	                <?php endif;?>
					</tr>
					<?php endforeach;?>
			</tbody>
		</table>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder;?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn;?>" />
		<?php echo JHtml::_('form.token');?>
	</div>
</form>


