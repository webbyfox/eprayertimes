<?php
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$document = JFactory::getDocument();
$document->addStyleSheet('components/com_eprayertimes/assets/css/eprayertimes.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){});
    Joomla.submitbutton = function(task) {
        if(task == 'import.cancel'){
            Joomla.submitform(task, document.getElementById('import-form'));
        } else {
            if (task != 'import.cancel' && document.formvalidator.isValid(document.id('import-form'))) {
                Joomla.submitform(task, document.getElementById('import-form'));
            } else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
            }
        }
    }
</script>
<h1><?php echo $this->mosque[0]->text;?></h1>
<form action="<?php echo JRoute::_('index.php?option=com_eprayertimes&layout=edit&id=' . (int) $this->item->id);?>" method="post" enctype="multipart/form-data" name="adminForm" id="import-form" class="form-validate">
    <div class="row-fluid">
        <div class="span4 form-horizontal">
            <fieldset class="adminform">
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('file');?></div>
                    <div class="controls"><?php echo $this->form->getInput('file');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('skipdata');?></div>
                    <div class="controls"><?php echo $this->form->getInput('skipdata');?></div>
                </div>
            </fieldset>
        </div>
        <div class="span8 form-horizontal">
            <div class="alert">
                <p>Format for CSV file should be as follows:<br>
                Islamic Date, date, fajr_begins, fajr_jamaat, sunrise, zuhr_begins, zuhr_jamaat, asr_begins, asr_jamaat, maghrib_begins, maghrib_jamaat, isha_begins, isha_jamaat<br>
                2, 21/01/2010, 06:26, 07:00, 08:06, 12:07, 12:45, 02:09, 02:30, 04:07, 04:07, 05:37, 08:00</p>
                <p>Islamic Date column is optional and can be left blank but must not be deleted.</p>
                <?php
if (count($this->samples) > 0) {
	echo '<p>You can use following example CSV files to edit and upload:</p>';
	foreach ($this->samples as $sample) {
		echo '<p><a href="' . JURI::base() . 'components/com_eprayertimes/samples/' . $sample . '">' . $sample . '</a></p>';
	}
}
?>
            </div>
        </div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="jform[id]" id="jform_id" value="<?php echo $this->mosque[0]->value;?>" />
        <?php echo JHtml::_('form.token');?>
    </div>
</form>