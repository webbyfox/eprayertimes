<?php
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$document = JFactory::getDocument();
$document->addStyleSheet('components/com_eprayertimes/assets/css/eprayertimes.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){});
    Joomla.submitbutton = function(task) {
        if(task == 'mosque.cancel'){
            Joomla.submitform(task, document.getElementById('mosque-form'));
        } else {
            if (task != 'mosque.cancel' && document.formvalidator.isValid(document.id('mosque-form'))) {
                Joomla.submitform(task, document.getElementById('mosque-form'));
            } else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
            }
        }
    }
</script>
<form action="<?php echo JRoute::_('index.php?option=com_eprayertimes&layout=edit&id=' . (int) $this->item->id);?>" method="post" enctype="multipart/form-data" name="adminForm" id="mosque-form" class="form-validate">
    <div class="row-fluid">
        <div class="span4 form-horizontal">
            <fieldset class="adminform">
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('name');?></div>
                    <div class="controls"><?php echo $this->form->getInput('name');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('alias');?></div>
                    <div class="controls"><?php echo $this->form->getInput('alias');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('type');?></div>
                    <div class="controls"><?php echo $this->form->getInput('type');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('state');?></div>
                    <div class="controls"><?php echo $this->form->getInput('state');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('street');?></div>
                    <div class="controls"><?php echo $this->form->getInput('street');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('county');?></div>
                    <div class="controls"><?php echo $this->form->getInput('county');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('city');?></div>
                    <div class="controls"><?php echo $this->form->getInput('city');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('postcode');?></div>
                    <div class="controls"><?php echo $this->form->getInput('postcode');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('country');?></div>
                    <div class="controls"><?php echo $this->form->getInput('country');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('age');?></div>
                    <div class="controls"><?php echo $this->form->getInput('age');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('wudhu_space');?></div>
                    <div class="controls"><?php echo $this->form->getInput('wudhu_space');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('toilets');?></div>
                    <div class="controls"><?php echo $this->form->getInput('toilets');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('parking_space');?></div>
                    <div class="controls"><?php echo $this->form->getInput('parking_space');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('staff_num');?></div>
                    <div class="controls"><?php echo $this->form->getInput('staff_num');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('description');?></div>
                    <div class="controls"><?php echo $this->form->getInput('description');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('created_by');?></div>
                    <div class="controls"><?php echo $this->form->getInput('created_by');?></div>
                </div>
            </fieldset>
        </div>
        <div class="span8 form-horizontal">
            <fieldset class="adminform">
<!--                <div class="control-group">
                    <div class="control-label"><?php //echo $this->form->getLabel('ramadhan_status'); ?></div>
                    <div class="controls"><?php //echo $this->form->getInput('ramadhan_status'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php //echo $this->form->getLabel('ramadhan_sdate'); ?></div>
                    <div class="controls"><?php //echo $this->form->getInput('ramadhan_sdate'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php //echo $this->form->getLabel('ramadhan_fdate'); ?></div>
                    <div class="controls"><?php //echo $this->form->getInput('ramadhan_fdate'); ?></div>
                </div>-->

                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('decimal_latitude');?></div>
                    <div class="controls"><?php echo $this->form->getInput('decimal_latitude');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('decimal_longitude');?></div>
                    <div class="controls"><?php echo $this->form->getInput('decimal_longitude');?></div>
                </div>
            </fieldset>
        </div>
        <input type="hidden" name="task" value="" />
        <?php echo $this->form->getInput('id');?>
        <?php echo JHtml::_('form.token');?>

    </div>
</form>