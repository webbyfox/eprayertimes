<?php

defined('_JEXEC') or die;
jimport('joomla.application.component.view');

class EprayertimesViewMosques extends JViewLegacy {
	protected $items;
	protected $pagination;
	protected $state;

	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			throw new Exception(implode("\n", $errors));
		}
		EprayertimesHelper::addSubmenu('mosques');
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/eprayertimes.php';
		$state = $this->get('State');
		$canDo = EprayertimesHelper::getActions($state->get('filter.category_id'));
		JToolBarHelper::title(JText::_('COM_EPRAYERTIMES_TITLE_MOSQUES'), 'mosques.png');
		//Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/mosque';
		if (file_exists($formPath)) {
			if ($canDo->get('core.create')) {
				JToolBarHelper::addNew('mosque.add', 'JTOOLBAR_NEW');
			}
			if ($canDo->get('core.edit') && isset($this->items[0])) {
				JToolBarHelper::editList('mosque.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state')) {
			if (isset($this->items[0]->state)) {
				JToolBarHelper::divider();
				JToolBarHelper::custom('mosques.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('mosques.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			} else if (isset($this->items[0])) {
				//If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'mosques.delete', 'JTOOLBAR_DELETE');
			}
			if (isset($this->items[0]->state)) {
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('mosques.archive', 'JTOOLBAR_ARCHIVE');
			}
			if (isset($this->items[0]->checked_out)) {
				JToolBarHelper::custom('mosques.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		//Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state)) {
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
				JToolBarHelper::deleteList('', 'mosques.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			} else if ($canDo->get('core.edit.state')) {
				JToolBarHelper::trash('mosques.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		JToolBarHelper::custom('import.import', 'upload.png', 'upload.png', 'Import', true);

		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_eprayertimes');
		}

		//Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_eprayertimes&view=mosques');
		$this->extra_sidebar = '';
		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_PUBLISHED'),
			'filter_published',
			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)
		);
	}

	protected function getSortFields() {
		return array(
			'a.name' => JText::_('COM_EPRAYERTIMES_MOSQUES_NAME'),
			'a.id' => JText::_('JGRID_HEADING_ID'),
			'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
			'a.state' => JText::_('JSTATUS'),
			'a.checked_out' => JText::_('COM_EPRAYERTIMES_MOSQUES_CHECKED_OUT'),
			'a.checked_out_time' => JText::_('COM_EPRAYERTIMES_MOSQUES_CHECKED_OUT_TIME'),
			'a.created_by' => JText::_('COM_EPRAYERTIMES_MOSQUES_CREATED_BY'),
		);
	}
}