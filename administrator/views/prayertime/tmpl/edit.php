<?php
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$document = JFactory::getDocument();
$document->addStyleSheet('components/com_eprayertimes/assets/css/eprayertimes.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){});
    Joomla.submitbutton = function(task) {
        if(task == 'mosque.cancel'){
            Joomla.submitform(task, document.getElementById('mosque-form'));
        } else {
            if (task != 'mosque.cancel' && document.formvalidator.isValid(document.id('mosque-form'))) {
                Joomla.submitform(task, document.getElementById('mosque-form'));
            } else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
            }
        }
    }
</script>
<form action="<?php echo JRoute::_('index.php?option=com_eprayertimes&layout=edit&id=' . (int) $this->item->id);?>" method="post" enctype="multipart/form-data" name="adminForm" id="mosque-form" class="form-validate">
    <div class="row-fluid">
        <div class="span4 form-horizontal">
            <fieldset class="adminform">
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('date');?></div>
                    <div class="controls"><?php echo $this->form->getInput('date');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('fajr_begins');?></div>
                    <div class="controls"><?php echo $this->form->getInput('fajr_begins');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('fajr_iqama');?></div>
                    <div class="controls"><?php echo $this->form->getInput('fajr_iqama');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('sunrise');?></div>
                    <div class="controls"><?php echo $this->form->getInput('sunrise');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('dhuhr_begins');?></div>
                    <div class="controls"><?php echo $this->form->getInput('dhuhr_begins');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('dhuhr_iqama');?></div>
                    <div class="controls"><?php echo $this->form->getInput('dhuhr_iqama');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('asr_begins');?></div>
                    <div class="controls"><?php echo $this->form->getInput('asr_begins');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('asr_iqama');?></div>
                    <div class="controls"><?php echo $this->form->getInput('asr_iqama');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('maghrib_begins');?></div>
                    <div class="controls"><?php echo $this->form->getInput('maghrib_begins');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('maghrib_iqama');?></div>
                    <div class="controls"><?php echo $this->form->getInput('maghrib_iqama');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('isha_begins');?></div>
                    <div class="controls"><?php echo $this->form->getInput('isha_begins');?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('isha_iqama');?></div>
                    <div class="controls"><?php echo $this->form->getInput('isha_iqama');?></div>
                </div>

                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('created_by');?></div>
                    <div class="controls"><?php echo $this->form->getInput('created_by');?></div>
                </div>
            </fieldset>
        </div>
        <input type="hidden" name="task" value="" />
        <?php echo $this->form->getInput('id');?>
        <?php echo JHtml::_('form.token');?>

    </div>
</form>