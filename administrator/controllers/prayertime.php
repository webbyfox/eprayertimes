<?php
/**
 * @package		KA Prayer Times
 * @author		Rizwan Mansuri http://www.webbyfox.co.uk
 * @copyright 	Copyright (C) 2015 Rizwan Mansuri - http://www.webbyfox.co.uk
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.controllerform');

class EprayertimesControllerPrayertime extends JControllerForm {
	function __construct() {
		$this->view_list = 'prayertimes';
		parent::__construct();
	}
}