<?php
/**
 * @package		e Prayer Times
 * @author		Rizwan Mansuri http://www.webbyfox.co.uk
 * @copyright 	Copyright (C) 2015 - Rizwan Mansuri - http://www.webbyfox.co.uk
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.controllerform');
jimport('joomla.filesystem.file');

class EprayertimesControllerImport extends JControllerForm {
	function __construct() {
		$this->view_list = 'import';
		parent::__construct();
	}
	function save() {
		$app = JFactory::getApplication();
		$model = $this->getModel('Import', 'EprayertimesModel');
//        $jinput = JFactory::getApplication()->input;
		//        $id = $jinput->get('id', '', 'INT');
		$data = JFactory::getApplication()->input->get('jform', array(), 'array');
		$file = JRequest::getVar('jform', array(), 'files', 'array');
		$redirect = 'index.php?option=com_eprayertimes&view=import&layout=edit&id=' . $data['id'];

		if (!empty($file['name']['file'])) {
			$fileInfo = pathinfo(JFile::makeSafe($file['name']['file']));
			$timestamp = time();
			$file['newname'] = $fileInfo['filename'] . '_' . $timestamp . '.' . $fileInfo['extension'];
			$result = $model->validatefile($file);
			if ($result) {
				$data['file'] = $file['newname'];
				if (!$model->parseCSVFile($data)) {
					$app->enqueueMessage(JText::_('COM_EPRAYERTIMES_IMPORT_FAILED'), 'error');
				}
			}
		}
		$this->setRedirect($redirect, 'Import completed.');
	}
	function import() {
		$cid = JFactory::getApplication()->input->get('cid', array(), 'array');
		$this->setRedirect('index.php?option=com_eprayertimes&view=import&layout=edit&id=' . $cid[0]);
	}
	function cancel() {
		$this->setRedirect('index.php?option=com_eprayertimes&view=mosques');
	}
}