<?php
/**
 * @package		KA Prayer Times
 * @author		Rizwan Mansuri http://www.webbyfox.co.uk
 * @copyright 	Copyright (C) 2015 Rizwan Mansuri - http://www.webbyfox.co.uk
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.controlleradmin');

class EprayertimesControllerMosques extends JControllerAdmin {
	public function getModel($name = 'mosque', $prefix = 'EprayertimesModel') {
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
	public function saveOrderAjax() {
		$input = JFactory::getApplication()->input;
		$pks = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');
		JArrayHelper::toInteger($pks);
		JArrayHelper::toInteger($order);
		$model = $this->getModel();
		$return = $model->saveorder($pks, $order);
		if ($return) {
			echo "1";
		}
		JFactory::getApplication()->close();
	}
}